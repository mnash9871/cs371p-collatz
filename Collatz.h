// ------------------------------
// projects/c++/collatz/Collatz.h
// Copyright (C) 2018
// Glenn P. Downing
// ------------------------------

#ifndef Collatz_h
#define Collatz_h

// --------
// includes
// --------

#include <iostream> // istream, ostream
#include <string>   // string
#include <utility>  // pair
#include <vector> //vector

using namespace std;

//global cache


// ---
// cch
// ---

/**
 * check the cache
 * @param num, the key
 * @return a value, cycle length of the key
 */
long long cch(long long num);

// -----
// place
// -----

/**
 * put a value in the cache
 * @param num index key
 * @param length the value that is the cycle length of num
 * @return void
 */
void place(long long num, long long length);

// ----
// even
// ----

/**
 * run the even case for collatz
 * @param num to divide by 2
 * @param len the cycle length to incr
 * @return true to make the compiler happy
 */
bool even(long long &num, long long &len);

// ---
// odd
// ---

/**
 * run the odd case for collatz
 * @param num to divide by 2
 * @param len the cycle length to incr
 * @return true to make the compiler happy
 */
bool odd(long long &num, long long &len);

// ----
// mcll
// ----

/**
 * inner loop for eval that computes cycle length of 1 number
 * @param num trying to find its cycle length
 * @return int cycle length of num
 */
long long mcll(long long num);

// ----------
// half_check
// ----------

/**
 * a half-step cache to check range
 * @param s the start of the range
 * @param e the end of the range
 * @return int max cycle length of the range
 */
int half_check(int s, int e);

// ------
// jump_i
// ------

/**
 * jump the iterator if there was a half-step cache hit
 * @param c the value returned by half_check
 * @return int the i that c should be at
 */
int jump_i(int c);

// ------------
// collatz_read
// ------------

/**
 * read two ints
 * @param s a string
 * @return a pair of ints, representing the beginning and end of a range, [i, j]
 */
pair<int, int> collatz_read (const string& s);

// ------------
// collatz_eval
// ------------

/**
 * @param i the beginning of the range, inclusive
 * @param j the end       of the range, inclusive
 * @return the max cycle length of the range [i, j]
 */
int collatz_eval (int i, int j);

// -------------
// collatz_print
// -------------

/**
 * print three ints
 * @param w an ostream
 * @param i the beginning of the range, inclusive
 * @param j the end       of the range, inclusive
 * @param v the max cycle length
 */
void collatz_print (ostream& w, int i, int j, int v);

// -------------
// collatz_solve
// -------------

/**
 * @param r an istream
 * @param w an ostream
 */
void collatz_solve (istream& r, ostream& w);

#endif // Collatz_h
