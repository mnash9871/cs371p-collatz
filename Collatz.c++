// --------------------------------
// projects/c++/collatz/Collatz.c++
// Copyright (C) 2018
// Glenn P. Downing
// --------------------------------

// --------
// includes
// --------

#include <cassert>  // assert
#include <iostream> // endl, istream, ostream
#include <sstream>  // istringstream
#include <string>   // getline, string
#include <utility>  // make_pair, pair
#include <vector> //vector

#include "Collatz.h"

using namespace std;

vector<long long> cache(10000000, -1);

// ------------
// collatz_read
// ------------

pair<int, int> collatz_read (const string& s)
{
    istringstream sin(s);
    int i;
    int j;
    sin >> i >> j;
    return make_pair(i, j);
}


//int total;

//check cache

long long cch(long long num)
{
    if ((num<=10000000) && (num>=0))
    {
        return cache.at(num);
    }
    else
    {
        return -1;
    }
}

//retrieve cache value

void place(long long num, long long length)
{
    if ((num<=10000000) && (num>=0))
    {
        cache.at(num) = length;
    }
}

//run even case

bool even(long long &num, long long &len)
{
    num = num >> 1;
    len = len + 1;
    return true;
}

//run odd case

bool odd(long long &num, long long &len)
{
    num = num + (num << 1) + 1;
    len = len + 1;
    return true;
}

//inner loop function, determines cycle length of a single num
long long mcll(long long num)
{
    long long ch = -1;
    long long len = 0;
    while (num > 1)
    {
        //check the cache
        ch = -1;
        ch = cch(num);
        if (ch != -1)
        {
            len = len + (ch-1);
            num = 1;
        }

        //odd or even case
        else
        {
            (num & 1 && odd(num, len)) || even(num, len);
        }

        //if it's the last step add 1
        if (num == 1)
        {
            len = len + 1;
            break;
        }
    }
    return len;
}

//order the input values
pair<long long, long long> order(int i, int j)
{
    if (j>=i)
    {
        return make_pair((long long)i, (long long)j);
    }
    else
    {
        return make_pair((long long)j, (long long)i);
    }
}

//check for a cached half-step value
int half_check(int s, int e)
{
    if (s<=837799 && 837799<=e)
    {
        return 525;
    }
    else if (s<=626331 && 626331<=e)
    {
        return 509;
    }
    else if (s<=511935 && 511935<=e)
    {
        return 470;
    }
    else if (s<=410011 && 410011<=e)
    {
        return 449;
    }
    else if (s<=230631 && 230631<=e)
    {
        return 443;
    }
    else if (s<=216367 && 216367<=e)
    {
        return 386;
    }
    else if (s<=156159 && 156159<=e)
    {
        return 383;
    }
    else if (s<=142587 && 142587<=e)
    {
        return 375;
    }
    else if (s<=106239 && 106239<=e)
    {
        return 354;
    }
    else if (s<=77031 && 77031<=e)
    {
        return 351;
    }
    else if (s<=52527 && 52527<=e)
    {
        return 340;
    }
    else if (s<=35655 && 35655<=e)
    {
        return 324;
    }
    else if (s<=34239 && 34239<=e)
    {
        return 311;
    }
    else if (s<=26623 && 26623<=e)
    {
        return 308;
    }
    else if (s<=23529 && 23529<=e)
    {
        return 282;
    }
    else if (s<=17647 && 17647<=e)
    {
        return 279;
    }
    else if (s<=13255 && 13255<=e)
    {
        return 276;
    }
    else if (s<=10971 && 10971<=e)
    {
        return 268;
    }
    else if (s<=6171 && 6171<=e)
    {
        return 262;
    }
    else if (s<=3711 && 3711<=e)
    {
        return 238;
    }
    else if (s<=2919 && 2919<=e)
    {
        return 217;
    }
    else if (s<=2463 && 2463<=e)
    {
        return 209;
    }
    else if (s<=2223 && 2223<=e)
    {
        return 183;
    }
    else if (s<=1161 && 1161<=e)
    {
        return 182;
    }
    else if (s<=871 && 871<=e)
    {
        return 179;
    }
    else if (s<=703 && 703<=e)
    {
        return 171;
    }
    else if (s<=649 && 649<=e)
    {
        return 145;
    }
    else if (s<=327 && 327<=e)
    {
        return 144;
    }
    else if (s<=313 && 313<=e)
    {
        return 131;
    }
    else if (s<=231 && 231<=e)
    {
        return 128;
    }
    else if (s<=171 && 171<=e)
    {
        return 125;
    }
    else if (s<=129 && 129<=e)
    {
        return 122;
    }
    else if (s<=97 && 97<=e)
    {
        return 119;
    }
    else if (s<=73 && 73<=e)
    {
        return 116;
    }
    else if (s<=54 && 54<=e)
    {
        return 113;
    }
    else if (s<=27 && 27<=e)
    {
        return 112;
    }
    else if (s<=25 && 25<=e)
    {
        return 24;
    }
    else if (s<=18 && 18<=e)
    {
        return 21;
    }
    else if (s<=9 && 9<=e)
    {
        return 20;
    }
    else if (s<=3 && 3<=e)
    {
        return 8;
    }
    else if (s<=2 && 2<=e)
    {
        return 2;
    }
    else if (s==1 && e==1)
    {
        return 1;
    }
    else
    {
        return -1;
    }
}

//soft switch case to jump the iterator in eval loop

int jump_i(int c)
{
    if (c == 1)
    {
        return 1;
    }
    else if (c == 2)
    {
        return 2;
    }
    else if (c == 8)
    {
        return 3;
    }
    else if (c == 20)
    {
        return 9;
    }
    else if (c ==21)
    {
        return 18;
    }
    else if (c == 24)
    {
        return 25;
    }
    else if (c == 112)
    {
        return 27;
    }
    else if (c == 113)
    {
        return 54;
    }
    else if (c == 116)
    {
        return 73;
    }
    else if (c == 119)
    {
        return 97;
    }
    else if (c == 122)
    {
        return 129;
    }
    else if (c == 125)
    {
        return 171;
    }
    else if (c == 128)
    {
        return 231;
    }
    else if (c == 131)
    {
        return 313;
    }
    else if (c == 144)
    {
        return 327;
    }
    else if (c == 145)
    {
        return 649;
    }
    else if (c == 171)
    {
        return 703;
    }
    else if (c == 179)
    {
        return 871;
    }
    else if (c == 182)
    {
        return 1161;
    }
    else if (c == 183)
    {
        return 2223;
    }
    else if (c == 209)
    {
        return 2463;
    }
    else if (c == 217)
    {
        return 2919;
    }
    else if (c == 238)
    {
        return 3711;
    }
    else if (c == 262)
    {
        return 6171;
    }
    else if (c == 268)
    {
        return 10971;
    }
    else if (c == 276)
    {
        return 13255;
    }
    else if (c == 279)
    {
        return 17647;
    }
    else if (c == 282)
    {
        return 23529;
    }
    else if (c == 308)
    {
        return 26623;
    }
    else if (c == 311)
    {
        return 34239;
    }
    else if (c == 324)
    {
        return 35655;
    }
    else if (c == 340)
    {
        return 52527;
    }
    else if (c == 351)
    {
        return 77031;
    }
    else if (c == 354)
    {
        return 106239;
    }
    else if (c == 375)
    {
        return 142587;
    }
    else if (c == 383)
    {
        return 156159;
    }
    else if (c == 386)
    {
        return 216367;
    }
    else if (c == 443)
    {
        return 230631;
    }
    else if (c == 449)
    {
        return 410011;
    }
    else if (c == 470)
    {
        return 511935;
    }
    else if (c == 509)
    {
        return 626331;
    }
    else if (c == 525)
    {
        return 837799;
    }
    else
    {
        return -1;
    }
}

// ------------
// collatz_eval
// ------------

int collatz_eval (int start, int end)
{
    cache.at(1) = 1;
    cache.at(2) = 2;
    long long greatest = -1;
    long long temp = -1;
    pair<long long, long long> se = order(start, end);
    long long s = se.first;
    long long e = se.second;
    for (long long i = s; i <= e; ++i)
    {
        temp = (long long) half_check((int)i, end);
        if (temp == -1) //if there wasn't a half-step cache load go to inner loop
        {
            temp = mcll(i);
        }
        else //if there was a half-step cache load, jump i forward
        {
            i = (long long) jump_i(temp);
        }
        place(i, temp); //place the value in cache
        if (temp > greatest) //check for greatest
        {
            greatest = temp;
        }
    }
    return (int)greatest;
}

// -------------
// collatz_print
// -------------

void collatz_print (ostream& w, int i, int j, int v)
{
    w << i << " " << j << " " << v << endl;
}

// -------------
// collatz_solve
// -------------

void collatz_solve (istream& r, ostream& w)
{
    string s;
    while (getline(r, s))
    {
        const pair<int, int> p = collatz_read(s);
        const int            i = p.first;
        const int            j = p.second;
        const int            v = collatz_eval(i, j);
        collatz_print(w, i, j, v);
    }
}
