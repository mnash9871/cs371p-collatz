// ------------------------------------
// projects/c++/collatz/TestCollatz.c++
// Copyright (C) 2018
// Glenn P. Downing
// ------------------------------------

// https://code.google.com/p/googletest/wiki/V1_7_Primer#Basic_Assertions

// --------
// includes
// --------

#include <iostream> // cout, endl
#include <sstream>  // istringtstream, ostringstream
#include <string>   // string
#include <utility>  // pair

#include "gtest/gtest.h"

#include "Collatz.h"

using namespace std;

// -----------
// TestCollatz
// -----------

// ----
// read
// ----

TEST(CollatzFixture, read)
{
    string s("1 10\n");
    const pair<int, int> p = collatz_read(s);
    ASSERT_EQ(p.first,   1);
    ASSERT_EQ(p.second, 10);
}

// -----
// cache
// -----
TEST(CollatzFixture, cch1)
{
    const int v = (int) cch(-1);
    ASSERT_EQ(v, -1);
}

// ------------
// even and odd
// ------------
TEST(CollatzFixture, odd1)
{
    long long t1 = 5;
    long long t2 = 1;
    odd(t1, t2);
    ASSERT_EQ((int) t1, 16);
}

TEST(CollatzFixture, even1)
{
    long long t1 = 4;
    long long t2 = 1;
    even(t1, t2);
    ASSERT_EQ((int) t1, 2);
}

TEST(CollatzFixture, half_check1)
{
    ASSERT_EQ(half_check(837799, 837799), 525);
    ASSERT_EQ(half_check(626331, 626331), 509);
    ASSERT_EQ(half_check(511935, 511935), 470);
    ASSERT_EQ(half_check(410011, 410011), 449);
    ASSERT_EQ(half_check(230631, 230631), 443);
    ASSERT_EQ(half_check(216367, 216367), 386);
    ASSERT_EQ(half_check(156159, 156159), 383);
    ASSERT_EQ(half_check(142587, 142587), 375);
    ASSERT_EQ(half_check(106239, 106239), 354);
    ASSERT_EQ(half_check(77031, 77031), 351);
    ASSERT_EQ(half_check(52527, 52527), 340);
    ASSERT_EQ(half_check(35655, 35655), 324);
    ASSERT_EQ(half_check(34239, 34239), 311);
    ASSERT_EQ(half_check(26623, 26623), 308);
    ASSERT_EQ(half_check(23529, 23529), 282);
    ASSERT_EQ(half_check(17647, 17647), 279);
    ASSERT_EQ(half_check(13255, 13255), 276);
    ASSERT_EQ(half_check(10971, 10971), 268);
    ASSERT_EQ(half_check(6171, 6171), 262);
    ASSERT_EQ(half_check(3711, 3711), 238);
    ASSERT_EQ(half_check(2919, 2919), 217);
    ASSERT_EQ(half_check(2463, 2463), 209);
    ASSERT_EQ(half_check(2223, 2223), 183);
    ASSERT_EQ(half_check(1161, 1161), 182);
    ASSERT_EQ(half_check(871, 871), 179);
    ASSERT_EQ(half_check(703, 703), 171);
    ASSERT_EQ(half_check(649, 649), 145);
    ASSERT_EQ(half_check(327, 327), 144);
    ASSERT_EQ(half_check(313, 313), 131);
    ASSERT_EQ(half_check(231, 231), 128);
    ASSERT_EQ(half_check(171, 171), 125);
    ASSERT_EQ(half_check(129, 129), 122);
    ASSERT_EQ(half_check(97, 97), 119);
    ASSERT_EQ(half_check(73, 73), 116);
    ASSERT_EQ(half_check(54, 54), 113);
    ASSERT_EQ(half_check(27, 27), 112);
    ASSERT_EQ(half_check(25, 25), 24);
    ASSERT_EQ(half_check(18, 18), 21);
    ASSERT_EQ(half_check(9, 9), 20);
    ASSERT_EQ(half_check(3, 3), 8);
    ASSERT_EQ(half_check(2, 2), 2);
    ASSERT_EQ(half_check(1, 1), 1);
}

TEST(CollatzFixture, jump_i1)
{
    ASSERT_EQ(jump_i(1), 1);
    ASSERT_EQ(jump_i(2), 2);
    ASSERT_EQ(jump_i(8), 3);
    ASSERT_EQ(jump_i(20), 9);
    ASSERT_EQ(jump_i(21), 18);
    ASSERT_EQ(jump_i(24), 25);
    ASSERT_EQ(jump_i(112), 27);
    ASSERT_EQ(jump_i(113), 54);
    ASSERT_EQ(jump_i(116), 73);
    ASSERT_EQ(jump_i(119), 97);
    ASSERT_EQ(jump_i(122), 129);
    ASSERT_EQ(jump_i(125), 171);
    ASSERT_EQ(jump_i(128), 231);
    ASSERT_EQ(jump_i(131), 313);
    ASSERT_EQ(jump_i(144), 327);
    ASSERT_EQ(jump_i(145), 649);
    ASSERT_EQ(jump_i(171), 703);
    ASSERT_EQ(jump_i(179), 871);
    ASSERT_EQ(jump_i(182), 1161);
    ASSERT_EQ(jump_i(183), 2223);
    ASSERT_EQ(jump_i(209), 2463);
    ASSERT_EQ(jump_i(217), 2919);
    ASSERT_EQ(jump_i(238), 3711);
    ASSERT_EQ(jump_i(262), 6171);
    ASSERT_EQ(jump_i(268), 10971);
    ASSERT_EQ(jump_i(276), 13255);
    ASSERT_EQ(jump_i(279), 17647);
    ASSERT_EQ(jump_i(282), 23529);
    ASSERT_EQ(jump_i(308), 26623);
    ASSERT_EQ(jump_i(311), 34239);
    ASSERT_EQ(jump_i(324), 35655);
    ASSERT_EQ(jump_i(340), 52527);
    ASSERT_EQ(jump_i(351), 77031);
    ASSERT_EQ(jump_i(354), 106239);
    ASSERT_EQ(jump_i(375), 142587);
    ASSERT_EQ(jump_i(383), 156159);
    ASSERT_EQ(jump_i(386), 216367);
    ASSERT_EQ(jump_i(443), 230631);
    ASSERT_EQ(jump_i(449), 410011);
    ASSERT_EQ(jump_i(470), 511935);
    ASSERT_EQ(jump_i(509), 626331);
    ASSERT_EQ(jump_i(525), 837799);
}

TEST(CollatzFixture, jump_i2)
{
    const int v = jump_i(-1);
    ASSERT_EQ(v, -1);
    ASSERT_EQ(jump_i(525), 837799);
}

// ----
// eval
// ----
TEST(CollatzFixture, eval_1)
{
    const int v = collatz_eval(1, 10);
    ASSERT_EQ(v, 20);
}

TEST(CollatzFixture, eval_2)
{
    const int v = collatz_eval(100, 200);
    ASSERT_EQ(v, 125);
}

TEST(CollatzFixture, eval_3)
{
    const int v = collatz_eval(201, 210);
    ASSERT_EQ(v, 89);
}

TEST(CollatzFixture, eval_4)
{
    const int v = collatz_eval(900, 1000);
    ASSERT_EQ(v, 174);
}

TEST(CollatzFixture, eval_5)
{
    const int v = collatz_eval(1, 7);
    ASSERT_EQ(v, 17);
}

TEST(CollatzFixture, eval_6)
{
    const int v = collatz_eval(1, 8);
    ASSERT_EQ(v, 17);
}

TEST(CollatzFixture, eval_7)
{
    const int v = collatz_eval(90000, 1);
    ASSERT_EQ(v, 351);
}

TEST(CollatzFixture, eval_8)
{
    const int v = collatz_eval(129, 129);
    ASSERT_EQ(v, 122);
}

// -----
// print
// -----

TEST(CollatzFixture, print)
{
    ostringstream w;
    collatz_print(w, 1, 10, 20);
    ASSERT_EQ(w.str(), "1 10 20\n");
}

// -----
// solve
// -----

TEST(CollatzFixture, solve)
{
    istringstream r("1 10\n100 200\n201 210\n900 1000\n");
    ostringstream w;
    collatz_solve(r, w);
    ASSERT_EQ("1 10 20\n100 200 125\n201 210 89\n900 1000 174\n", w.str());
}
